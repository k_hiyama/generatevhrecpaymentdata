package com.ipet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class GenerateVhRecPaymentData {

	protected static Properties props;	
	protected static String env;
	protected static QueryRunner runnerMa;	
	protected static ObjectMapper mapper = new ObjectMapper();
	
	// 取得する窓口精算開始日
	public static String targetDate = "2019-04-30";

	public static void main(String args[]) throws Exception {
		// Database接続設定
		base();
		// csvファイル読み込み
		List<CSVRecord> csvRecords = readInputCsv();
		// ma.policyテーブルからデータ取得
		List<Map<String, Object>> results = getMaData(csvRecords);
		// csvファイル出力
		outPutCsv(results);
	}
	
	// Database接続情報の取得
	public static void base() throws Exception {
		props = new Properties();
		FileInputStream propFile = new FileInputStream("conf/app.properties");
		props.load(propFile);
		runnerMa = createQueryRunner("ma");
		env = props.getProperty("env");
		if (StringUtils.isBlank(env)) {
			env = "development";
		}
	}
	
	/**
	 * 入力用CSVファイルから値を取得する
	 * @return CSVファイルから取得したレコード
	 */
	public static List<CSVRecord> readInputCsv() throws Exception {
		File csv = new File("csv/input/input.csv");
		FileReader fr = new FileReader(csv);
		CSVParser parser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(fr);
		List<CSVRecord> csvRecords = parser.getRecords();
		
		return csvRecords;
	}

	/**
	 * DB(ma.policy)からデータ取得する。
	 * @param 入力用CSVファイルから取得したペットオーナID
	 * @return DBから取得したペットオーナIDに紐つくレコード
	 */
	public static List<Map<String, Object>> getMaData(List<CSVRecord> inputData) throws Exception {
		List<CSVRecord> petOwnerIds = inputData;
		String sql =  String.format("SELECT * FROM policy WHERE pet_owner_id in (%s)", implode(petOwnerIds));
		List<Map<String, Object>> results = runnerMa.query(sql, new MapListHandler());
		return results;
	}
	
	/**
	 * databaseから取得した値をCSV形式に変換してファイルに出力する。
	 * @param results ma.policyテーブルから取得したレコード
	 */
	public static void outPutCsv(List<Map<String, Object>> results) {
		
		String fileName = "csv/output/".concat(getOutputCsvFileName());
//		String encode = "UTF-8";
		String encode = "SHIFT-JIS";
		
		try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), encode))) {
			CSVPrinter printer = CSVFormat
				.EXCEL
				// 多頭飼いによってペットオーナが複数のレコードを持っているケースがあるので、識別を容易にするために証券番号も取得する
				// .withHeader("pet_owner_id", "policy_number", "plan", "payment_type", "start_date", "vh_rec_payment_date", "contract_date")
				.withHeader("ペットオーナID", "証券番号", "商品", "払込方法", "始期日", "窓口精算開始日", "計上日")
				.print(bw);
			
			for (Map<String, Object> result : results) {
				String infoJson = (String) result.get("info");
				Map<String, String> jsonMap = mapper.readValue(infoJson, new TypeReference<TreeMap<String, String>>(){});
				
				// 窓口精算開始日が指定した日付で現在有効なレコードを出力対象とする
				if (jsonMap.containsKey("vhRecPaymentDate") && StringUtils.equals(jsonMap.get("vhRecPaymentDate"), targetDate) && (boolean) result.get("is_valid_now") == true ) {
					printer.printRecord(result.get("pet_owner_id"),
							result.get("policy_number"),
							jsonMap.get("plan"),
							jsonMap.get("paymentType"),
							jsonMap.get("startDate"),
							jsonMap.get("vhRecPaymentDate"),
							jsonMap.get("contractDate"));
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("CSVファイル生成完了 : " + (new File(fileName).getAbsolutePath()));
	}
		
	private static QueryRunner createQueryRunner(String data) {
		MysqlDataSource dataSource = new MysqlDataSource();
		
		dataSource.setUrl(
				String.format(
						"jdbc:mysql://%s:%s/%s?autoReconnect=true&failOverReadOnly=false&maxReconnects=20",
						props.getProperty(data + ".mysql.ip"),
						props.getProperty(data + ".mysql.port"),
						props.getProperty(data + ".mysql.database")
						)
				);
		dataSource.setUser(props.getProperty(data + ".mysql.user"));
		dataSource.setPassword(props.getProperty(data + ".mysql.password"));
		
		return new QueryRunner(dataSource);
	}
	
	/**
	 * 配列内の文字列を接続文字で繋いだ文字列を返却する。
	 * @param pieces 文字列の配列
	 * @return 配列内の文字列を接続文字で繋いだ文字列
	 */
	public static String implode(List<CSVRecord> pieces) {
		StringBuilder sb = new StringBuilder();
		for (CSVRecord piece : pieces) {
			sb.append(String.format("'%s',", piece.get(0)));
		}
		return sb.substring(0, sb.length() - 1);
	}
	
	/**
	 * 出力用CSVファイル名称の設定する
	 * @return CSVファイル名(output_[yyyyMMdd_HHmmssSSS].csv)
	 */
	private static String getOutputCsvFileName() {
		Calendar cl = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
		StringBuilder buff = new StringBuilder();
		buff.append("output_");
		buff.append(sdf.format(cl.getTime()));
		buff.append(".csv");
		return buff.toString();
	}
}
